"""
Demander des informations sur la notion de curseur.
https://docs.python.org/3/library/sqlite3.html

Pour afficher le chemin de la bd associé à une connexion
for id_, name, filename in sport_db.execute("PRAGMA database_list"): print (id_, name, filename)
https://stackoverflow.com/questions/13912731/python-sqlite3-get-sqlite-connection-path
"""


def get_nb_sport(conn):
    req = conn.execute("SELECT count(*) FROM sports")
    res = req.fetchone()
    return res if res is None else res[0]


def get_index(conn, value):
    req = conn.execute("SELECT id FROM sports WHERE sport = ?", (value,))
    res = req.fetchone()
    return res if res is None else res[0]


def add_sport(conn, sport, nb_videos):
    conn.execute(
        "INSERT INTO sports (sport, nb_videos) VALUES (?, ?)", (sport, nb_videos)
    )
    conn.commit()


def test_get_index(sport_not_empty_db):
    index = get_index(sport_not_empty_db, "ping pong")
    assert index == 2


def test_empty_db(sport_db):
    nb_sports = get_nb_sport(sport_db)
    assert nb_sports == 0


def test_add_sport(sport_db, sports_list):
    for sport in sports_list:
        add_sport(sport_db, sport["sport"], sport["nb_videos"])
    assert get_nb_sport(sport_db) == 3
