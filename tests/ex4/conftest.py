from pathlib import Path
import sqlite3
import pytest
from tempfile import TemporaryDirectory


@pytest.fixture(scope="session")
def sport_db():
    with TemporaryDirectory() as db_dir:
        db_path = Path(db_dir) / "sports.db"
        conn = sqlite3.connect(db_path)
        # cur = conn.cursor()
        conn.execute(
            "CREATE TABLE sports(id integer primary key autoincrement, sport, nb_videos integer)"
        )

        yield conn

        conn.close()


@pytest.fixture(scope="function")
def sport_not_empty_db(sport_db):
    data = [("swimming", 100), ("ping pong", 50), ("synchronized swimming", 15)]
    sport_db.executemany("INSERT INTO sports (sport, nb_videos) VALUES (?, ?)", data)
    sport_db.commit()

    yield sport_db

    sport_db.execute("DELETE FROM sports")


@pytest.fixture()
def sports_list():
    return [
        {"sport": "swimming", "nb_videos": 100},
        {"sport": "ping pong", "nb_videos": 50},
        {"sport": "synchronized swimming", "nb_videos": 15},
    ]
