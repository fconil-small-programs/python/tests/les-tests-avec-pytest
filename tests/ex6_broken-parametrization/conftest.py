import pytest


@pytest.fixture
def video_input(request):
    return request.param


def pytest_addoption(parser):
    parser.addoption(
        "--video_path",
        action="append",
        default=[],
        help="list of video path to pass to test functions",
    )
