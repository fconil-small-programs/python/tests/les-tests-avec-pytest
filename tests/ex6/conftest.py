import pytest


@pytest.fixture(
    params=[
        "samples/video1.mp4",
        "samples/video2.mp4",
        "samples/video3.mp4",
    ]
)
def video_input(request):
    return request.param


@pytest.fixture(
    params=[
        {"video": "samples/video1.mp4", "duration": 100, "frames": 2400},
        {"video": "samples/video2.mp4", "duration": 15, "frames": 360},
        {"video": "samples/video3.mp4", "duration": 5, "frames": 120},
    ]
)
def video_frames_input(request):
    return request.param


@pytest.fixture(
    params=[
        {"video": "samples/video1.mp4", "duration": 100, "frames": 2400},
        {"video": "samples/video2.mp4", "duration": 15, "frames": 360},
        {"video": "samples/video3.mp4", "duration": 5, "frames": 120},
    ],
    ids=["video1_d-100_f-2400", "video2_d-15_f-360", "video3_d-5_f-120"],
)
def video_frames_input_with_ids(request):
    return request.param


def pytest_addoption(parser):
    parser.addoption(
        "--video_path",
        action="append",
        default=[],
        help="list of video path to pass to test functions",
    )
