import pytest


def get_nb_frames(video, duration):
    return duration * 24


@pytest.mark.parametrize(
    "video, duration, nb_frames",
    [
        ("samples/video1.mp4", 100, 2400),
        ("samples/video2.mp4", 15, 360),
        ("samples/video3.mp4", 5, 120),
    ],
)
def test_get_nb_frames(video, duration, nb_frames):
    nbf = get_nb_frames(video, duration)
    assert nbf == nb_frames
