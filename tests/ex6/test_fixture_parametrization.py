def get_video_duration(video):
    return 20


def test_get_video_duration(video_input):
    video = video_input
    assert get_video_duration(video) == 20


def get_nb_frames(video, duration):
    return duration * 24


def test_get_nb_frames(video_frames_input):
    nbf = get_nb_frames(video_frames_input["video"], video_frames_input["duration"])
    assert nbf == video_frames_input["frames"]


def test_get_nb_frames_with_ids(video_frames_input_with_ids):
    nbf = get_nb_frames(
        video_frames_input_with_ids["video"], video_frames_input_with_ids["duration"]
    )
    assert nbf == video_frames_input_with_ids["frames"]
