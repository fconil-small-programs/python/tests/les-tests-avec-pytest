import pytest


def get_index(list_dict, key, value):
    """helper to get index of a key in a json file"""
    for i in range(len(list_dict)):
        if list_dict[i][key] == value:
            return i


@pytest.fixture()
def list_sports():
    return [
        {"sport": "swimming", "nb_videos": 100},
        {"sport": "ping pong", "nb_videos": 50},
        {"sport": "synchronized swimming", "nb_videos": 15},
    ]


def test_get_index(list_sports):
    index = get_index(list_sports, "sport", "ping pong")
    assert index == 1
