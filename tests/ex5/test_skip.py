import os

import pytest


def sound_gap_measure(video_ref, video_new):
    # Function that is not ready yet
    return 0


@pytest.mark.skip(reason="Function is not coded yet")
def test_no_sound_gap():
    # GIVEN a reference video, compute the gap with a given video using sound
    video_ref = "sample/video_ref.mp4"
    video_new = "sample/video_new.mp4"

    # WHEN we compute the gap between the video using the starter sound
    gap = sound_gap_measure(video_ref, video_new)

    # THEN there should be no difference
    assert gap == 0


def extract_frame(video, frame):
    # Fake function
    pass


@pytest.mark.skipif(os.cpu_count() < 8, reason="Your computer is not powerfull enough")
def test_extract_frame(tmp_path):
    # GIVEN a video
    video = "sample/video.mp4"
    frame = "sample/frame.jpg"

    # WHEN we extract a frame from the video
    extract_frame(video, frame)

    # THEN the frame should exist after the extraction
    assert frame.exists()
