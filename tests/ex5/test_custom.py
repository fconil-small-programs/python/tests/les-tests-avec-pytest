import time

import pytest


def get_nb_frames(video):
    time.sleep(2)
    return 100


def extract_swimmer(video):
    time.sleep(2)
    return {"x": 12, "y": 34, "pos": 5, "meta_1": "val_1"}


def get_video_duration(video):
    return 20


@pytest.mark.slow
def test_get_nb_frames():
    video = "sample/video.mp4"
    nb_frames = get_nb_frames(video)
    assert nb_frames == 100


@pytest.mark.slow
def test_extract_swimmer():
    video = "sample/video.mp4"
    swimmer = extract_swimmer(video)
    assert swimmer == {"x": 12, "y": 34, "pos": 5, "meta_1": "val_1"}


def test_get_video_duration():
    video = "sample/video.mp4"
    duration = get_video_duration(video)
    assert duration == 20
