import sys

import pytest


def get_windows_version():
    return 10


def get_system():
    return sys.platform()


@pytest.mark.xfail(reason="I do not have a windows system")
def test_run_on_windows():
    platform = get_system()
    assert platform == "win32"


@pytest.mark.xfail(reason="We should required Windows 11")
def test_windows_version():
    version = get_windows_version()
    assert version >= 10


@pytest.mark.xfail(reason="We should required Windows 11", strict=True)
def test_windows_version_strict():
    version = get_windows_version()
    assert version >= 10
