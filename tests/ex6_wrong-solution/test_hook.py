"""
pytest_generate_tests did not work for video_input if defined in conftest.py or in this file (test_hook.py)

$ pytest -v --no-header --video_type="ogg" test_hook.py
============================================================================ test session starts =============================================================================
collected 0 items / 1 error

=================================================================================== ERRORS ===================================================================================
_____________________________________________________________________ ERROR collecting ex6/test_hook.py ______________________________________________________________________
../../.venv/lib/python3.10/site-packages/_pytest/runner.py:341: in from_call
    result: Optional[TResult] = func()
../../.venv/lib/python3.10/site-packages/_pytest/runner.py:372: in <lambda>
    call = CallInfo.from_call(lambda: list(collector.collect()), "collect")
../../.venv/lib/python3.10/site-packages/_pytest/python.py:534: in collect
    return super().collect()
../../.venv/lib/python3.10/site-packages/_pytest/python.py:455: in collect
    res = ihook.pytest_pycollect_makeitem(
../../.venv/lib/python3.10/site-packages/pluggy/_hooks.py:433: in __call__
    return self._hookexec(self.name, self._hookimpls, kwargs, firstresult)
../../.venv/lib/python3.10/site-packages/pluggy/_manager.py:112: in _hookexec
    return self._inner_hookexec(hook_name, methods, kwargs, firstresult)
../../.venv/lib/python3.10/site-packages/_pytest/python.py:271: in pytest_pycollect_makeitem
    return list(collector._genfunctions(name, obj))
../../.venv/lib/python3.10/site-packages/_pytest/python.py:498: in _genfunctions
    self.ihook.pytest_generate_tests.call_extra(methods, dict(metafunc=metafunc))
../../.venv/lib/python3.10/site-packages/pluggy/_hooks.py:489: in call_extra
    return self._hookexec(self.name, hookimpls, kwargs, firstresult)
../../.venv/lib/python3.10/site-packages/pluggy/_manager.py:112: in _hookexec
    return self._inner_hookexec(hook_name, methods, kwargs, firstresult)
../../.venv/lib/python3.10/site-packages/_pytest/fixtures.py:1570: in pytest_generate_tests
    metafunc.parametrize(
../../.venv/lib/python3.10/site-packages/_pytest/python.py:1347: in parametrize
    newcallspec = callspec.setmulti(
../../.venv/lib/python3.10/site-packages/_pytest/python.py:1152: in setmulti
    raise ValueError(f"duplicate {arg!r}")
E   ValueError: duplicate 'video_type'
========================================================================== short test summary info ===========================================================================
ERROR test_hook.py - ValueError: duplicate 'video_type'
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Interrupted: 1 error during collection !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
============================================================================== 1 error in 0.39s ==============================================================================

Conclusion : les fixtures n'ont pas besoin d'être dans conftest.py pour être collectées. A étudier !!!!
"""


def get_video_duration(video):
    return 20


def pytest_generate_tests(metafunc):
    if "video_input" in metafunc.fixturenames:
        metafunc.parametrize("video_input", metafunc.config.getoption("video_path"))


def test_get_dynamic_video_duration(video_input):
    video = video_input
    assert get_video_duration(video) == 20
