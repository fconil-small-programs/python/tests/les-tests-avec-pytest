import pytest


def get_video_duration(video):
    return 20


@pytest.fixture(
    params=[
        "samples/video1.mp4",
        "samples/video2.mp4",
        "samples/video3.mp4",
    ]
)
def video_input(request):
    return request.param


def test_get_video_duration(video_input):
    video = video_input
    assert get_video_duration(video) == 20
