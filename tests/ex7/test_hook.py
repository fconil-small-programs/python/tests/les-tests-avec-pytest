"""
pytest_generate_tests ne fonctionne pas si la fixture video_input est définie
dans conftest.py ou dans le fichier courant (test_hook.py)

https://github.com/pytest-dev/pytest/issues/2726

$ pytest -v --no-header --video_path="samples/video4.mp4" tests/ex7/test_hook.py
"""


def get_video_duration(video):
    return 20


def pytest_generate_tests(metafunc):
    if "video_input" in metafunc.fixturenames:
        metafunc.parametrize("video_input", metafunc.config.getoption("video_path"))


def test_get_dynamic_video_duration(video_input):
    video = video_input
    assert get_video_duration(video) == 20
