from pathlib import Path
import sqlite3
import pytest
from tempfile import TemporaryDirectory


def get_nb_sport(conn):
    req = conn.execute("SELECT count(*) FROM sports")
    res = req.fetchone()
    return res if res is None else res[0]


@pytest.fixture()
def sport_db():
    with TemporaryDirectory() as db_dir:
        db_path = Path(db_dir) / "sports.db"
        conn = sqlite3.connect(db_path)
        # cur = conn.cursor()
        conn.execute(
            "CREATE TABLE sports(id integer primary key autoincrement, sport, nb_videos integer)"
        )

        yield conn

        conn.close()


def test_empty_db(sport_db):
    nb_sports = get_nb_sport(sport_db)
    assert nb_sports == 0
