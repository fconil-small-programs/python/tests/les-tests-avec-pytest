"""
Demander des informations sur la notion de curseur.
https://docs.python.org/3/library/sqlite3.html

Pour afficher le chemin de la bd associé à une connexion
for id_, name, filename in sport_db.execute("PRAGMA database_list"): print (id_, name, filename)
https://stackoverflow.com/questions/13912731/python-sqlite3-get-sqlite-connection-path
"""

from pathlib import Path
import sqlite3
import pytest
from tempfile import TemporaryDirectory


def get_nb_sport(conn):
    req = conn.execute("SELECT count(*) FROM sports")
    res = req.fetchone()
    return res if res is None else res[0]


def get_index(conn, value):
    req = conn.execute("SELECT id FROM sports WHERE sport = ?", (value,))
    res = req.fetchone()
    return res if res is None else res[0]


@pytest.fixture(scope="module")
def sport_db():
    with TemporaryDirectory() as db_dir:
        db_path = Path(db_dir) / "sports.db"
        conn = sqlite3.connect(db_path)
        # cur = conn.cursor()
        conn.execute(
            "CREATE TABLE sports(id integer primary key autoincrement, sport, nb_videos integer)"
        )

        yield conn

        conn.close()


@pytest.fixture(scope="function")
def sport_not_empty_db(sport_db):
    data = [("swimming", 100), ("ping pong", 50), ("synchronized swimming", 15)]
    sport_db.executemany("INSERT INTO sports (sport, nb_videos) VALUES (?, ?)", data)
    sport_db.commit()

    yield sport_db

    sport_db.execute("DELETE FROM sports")


def test_get_index(sport_not_empty_db):
    index = get_index(sport_not_empty_db, "ping pong")
    assert index == 2


def test_empty_db(sport_db):
    nb_sports = get_nb_sport(sport_db)
    assert nb_sports == 0
