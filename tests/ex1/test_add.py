"""Simplest pytest test
https://docs.pytest.org/en/stable/getting-started.html#install-pytest
"""


def add(n):
    return n + 1


def test_add_one():
    # Expect a function adding 1 to the number passed
    assert add(3) == 4


def test_add_two():
    # Expect a function adding 2 to the number passed
    assert add(3) == 5


if __name__ == "__main__":
    test_add_one()
    test_add_two()
